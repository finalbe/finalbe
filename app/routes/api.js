const express = require("express");
const router = express.Router();
const fs = require('fs')
const { user, product, auth, order } = require("../controllers/")
const multer = require('multer')
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        const { merchant_id, name } = req.body
        const dir = `uploads/products-${merchant_id}/${name}`

        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir, { recursive: true })
        }

        cb(null, dir)
    },
    filename: (req, file, cb) => {
        const filename = file.originalname
        const fileSplit = filename.split('.')
        const fileExt = fileSplit[fileSplit.length - 1]
        const uniqueSuffix = Date.now() + '-' + (Math.round(Math.random() * 1E9).toString() + '.' + fileExt)
        cb(null, file.fieldname + '-' + uniqueSuffix)
    }
})
const upload = multer({ storage })

// All User
router.post("/registration", user.register);
router.get("/login", user.login);

// Products
router.post("/product", auth.authorize, upload.array('products', 4), product.create)
router.get("/product", auth.authorize, product.list)
router.get("/product/:id", auth.authorize, product.find)

// Orders
router.post("/orders", auth.authorize, order.create)

module.exports = router;
