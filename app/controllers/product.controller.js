const db = require("../models");
const fs = require("fs")
const sequelize = require("sequelize")
const { Op } = require("sequelize");
const PRODUCTS_MODEL = db.products;
const CATEGORIES_MODEL = db.category;
const CUSOTMERS_MODEL = db.customers;

function deleteFileExceptMostRecent(product, files = []) {
    const dir = `uploads/products-${product.merchant_id}/${product.name}/`
    fs.readdir(dir, function (err, items) {
        const deletedFile = files.filter((file) => items.find(item => file.filename === item))

        deletedFile.forEach((file) => {
            fs.unlink(dir + file.filename, (error) => {
                if (error) throw error
                console.log('Deleted file ' + dir + file.filename)
            })
        })
    })
}

function deleteProductName(product) {
    const dir = `uploads/products-${product.merchant_id}/${product.name}`
    fs.rmdir(dir, { recursive: true, force: true }, (error) => {
        if (error) throw error
        console.log('Deleted direktory ' + dir)
    })
}

function deleteFolder(id) {
    const dir = `uploads/products-${id}/`
    fs.rmdir(dir, { recursive: true, force: true }, (error) => {
        if (error) throw error
        console.log('Deleted direktory ' + dir)
    })
}

async function create(req, res) {
    const { name, merchant_id, price, category_id } = req.body

    try {
        if (!name || !merchant_id || !price || !category_id) {

            if (!merchant_id) {
                deleteFolder(merchant_id)
            } else {
                deleteProductName(req.body)
            }

            res.statusCode = 400
            res.json({
                status: false,
                message: "Nama, merchant id, harga, dan kategori harus diisi!"
            })

            return
        }

        let query = {
            where: {
                id: merchant_id
            }
        }

        const customerExist = await CUSOTMERS_MODEL.findOne(query)

        if (!customerExist) {
            res.statusCode = 404
            res.json({
                status: false,
                message: "ID Merchant tidak ditemukan! mohon masukkan yang benar"
            })

            deleteFolder(merchant_id)

            return
        }

        query = {
            where: {
                id: category_id
            }
        }

        const categoryExist = await CATEGORIES_MODEL.findOne(query)

        if (!categoryExist) {
            res.statusCode = 404
            res.json({
                status: false,
                message: "ID Kategori tidak ditemukan! mohon masukkan yang benar"
            })

            deleteProductName(req.body)

            return
        }

        const product = await PRODUCTS_MODEL.create({
            name,
            merchant_id,
            price,
            category_id,
            image: `uploads/products-${merchant_id}/${name}`
        })
        res.statusCode = 201
        res.json({
            status: true,
            message: "Berhasil membuat produk",
            data: product
        })
    } catch (error) {
        console.log(error)
        res.statusCode = 500
        res.json({
            status: false,
            message: error.message
        })
    }
}

async function get(req, res) {
    try {
        const query = req.query
        console.log(query)
        const response = await PRODUCTS_MODEL.findAll({
            where: {
                category_id: (query.category) ? {
                    [Op.eq]: query.category,
                } : {
                    [Op.in]: [1, 2, 3, 4, 5],
                }
            },
            include: [
                {
                    model: CUSOTMERS_MODEL,
                    attributes: ["id", "full_name", "email", "image", "phone", "address", "city"]
                },
                {
                    model: CATEGORIES_MODEL,
                    attributes: ["id", "name", "description"]
                }
            ]
        })

        if (response.length === 0) {
            res.json({
                status: false,
                message: "Data produk kosong"
            })

            return
        }

        const products = response.map(arr => ({
            id: arr.id,
            name: arr.name,
            merchant: arr.customer,
            price: arr.price,
            category: arr.category,
            image: arr.image,
            createdAt: arr.createdAt,
            updatedAt: arr.updatedAt,

        }))
        res.json({
            status: true,
            message: "Berhasil mendapatkan produk",
            data: products
        })
    } catch (error) {
        console.log(error)
        res.statusCode = 500
        res.json({
            status: false,
            message: error.message
        })
    }
}

async function find(req, res) {
    const { id } = req.params
    try {
        const response = await PRODUCTS_MODEL.findByPk(id, {
            include: [
                {
                    model: CUSOTMERS_MODEL,
                    attributes: ["id", "full_name", "email", "image", "phone", "address", "city"]
                },
                {
                    model: CATEGORIES_MODEL,
                    attributes: ["id", "name", "description"]
                }
            ]
        })

        if (!response) {
            res.statusCode = 404
            res.json({
                status: false,
                message: "Data produk tidak ada"
            })

            return
        }

        const product = {
            id: response.id,
            name: response.name,
            merchant: response.customer,
            price: response.price,
            category: response.category,
            image: response.image,
            createdAt: response.createdAt,
            updatedAt: response.updatedAt,

        }

        res.json({
            status: true,
            message: "Produk berhasil ditemukan",
            data: product
        })
    } catch (error) {
        console.log(error)
        res.json({
            status: false,
            message: error.message
        })
    }
}

module.exports = {
    create,
    list: get,
    find
}