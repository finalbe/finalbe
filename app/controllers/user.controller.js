const auth = require("./auth.controller.js");
const db = require("../models");
const CUSTOMERS_MODEL = db.customers;

function validateEmail(email) {
  return email.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
}

async function register(req, res) {
  try {
    const fname = req.body.full_name;
    const email = req.body.email.toLowerCase();
    const password = req.body.password;

    if (!fname || !email || !password) {
      res.statusCode = 400
      res.json({
        status: false,
        message: "Nama lengkap, email, dan password tidak boleh kosong!"
      })

      return
    }

    if (!validateEmail(email)) {
      res.statusCode = 400
      res.json({
        status: false,
        message: "Email yang dimasukkan tidak valid"
      })

      return
    }

    if (password.length < 8) {
      res.statusCode = 400
      res.json({
        status: false,
        message: "Password minimal 8 karakter!"
      })

      return
    }

    const encryptPassword = await auth.encryptPassword(password);

    const existUser = await CUSTOMERS_MODEL.findOne({
      where: {
        email: email,
      },
    });

    if (existUser) {
      return res.status(401).json({
        message: "Email sudah digunakan!",
      });
    }
    const user = await CUSTOMERS_MODEL.create({
      full_name: fname,
      email: email,
      password: encryptPassword,
      createdAt: new Date(),
      updatedAt: new Date(),
    })

    const token = auth.createToken({
      id: user.id,
      email: user.email,
    });

    res.statusCode = 200
    res.json({
      status: true,
      message: "Berhasil membuat user",
      token,
      data: user
    })

  } catch (error) {
    console.log(error);
    res.statusCode = 500
    res.json({
      status: false,
      message: error.message
    })
  }
}

async function login(req, res) {
  try {
    const email = req.body.email.toLowerCase();
    const password = req.body.password;

    const user = await CUSTOMERS_MODEL.findOne({
      where: {
        email: email,
      },
    });

    if (!user) {
      return res.status(401).json({
        message: "Email tidak ditemukan!",
      });
    }

    const matchPass = await auth.checkPassword(user.password, password);

    if (!matchPass) {
      return res.status(401).json({
        message: "Password anda salah!",
      });
    }

    const token = auth.createToken({
      id: user.id,
      email: user.email,
    });

    return res.status(200).json({
      message: "User berhasil login",
      data: user,
      token: token,
    });
  } catch (error) {
    console.log(error);
  }
}

async function profile(req, res) {
  try {
    const profile = await CUSTOMERS_MODEL.find(req.params.id);

    if (!profile) {
      return res.status(401).json({
        message: "Profile user tidak ditemukan!",
      });
    }

    return res.status(200).json({
      message: "Profile user berhasil ditemukan!",
      data: profile,
    });
  } catch (error) {
    console.log(error);
  }
}

module.exports = {
  register,
  login,
  profile,
};
