const main = require("./main");
const auth = require('./auth.controller')
const user = require("./user.controller")
const product = require("./product.controller")
const order = require("./order.controller")

module.exports = {
  main,
  auth,
  user,
  product,
  order,
};

