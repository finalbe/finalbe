const db = require('../models')
const ORDER_MODEL = db.order
const PRODUCTS_MODEL = db.products
const BUYERS_MODEL = db.customers

async function create(req, res) {
    const { buyer_id, product_id, fix_value, quantity, status = "pending" } = req.body
    try {
        if (!buyer_id || !product_id) {
            res.statusCode = 400
            res.json({
                status: false,
                message: "Buyer ID dan Product ID tidak boleh kosong"
            })

            return
        }

        const buyer = await BUYERS_MODEL.findByPk(buyer_id)
        console.log('buyer: ', buyer)
        if (!buyer) {
            res.statusCode = 400
            res.json({
                status: false,
                message: "Buyer ID tidak ditemukan!"
            })

            return
        }

        const product = await PRODUCTS_MODEL.findByPk(product_id)

        if (!product) {
            res.statusCode = 400
            res.json({
                status: false,
                message: "Product ID tidak ditemukan!"
            })
        }

        if (product.merchant_id === buyer_id) {
            res.statusCode = 400
            res.json({
                status: false,
                message: "Tidak boleh membeli produk yang dijual sendiri!"
            })
        }

        if (fix_value < 1000) {
            res.statusCode = 400
            res.json({
                status: false,
                message: "Harga yang ditawar tidak boleh kurang dari 1.000 Rupiah"
            })

            return
        }

        if (quantity <= 0) {
            res.statusCode = 400
            res.json({
                status: false,
                message: "Jumlah yang dibeli tidak boleh kurang dari 0"
            })

            return
        }

        const order = await ORDER_MODEL.create({
            buyer_id,
            product_id,
            fix_value,
            quantity,
            quantity,
            status
        })

        res.statusCode = 201
        res.json({
            status: true,
            message: "Orderan berhasil dibuat",
            data: order
        })

    } catch (error) {
        console.log(error)
        req.statusCode = 500
        req.json({
            status: false,
            title: error.name,
            message: error.message
        })
    }
}

module.exports = {
    create,
}