'use strict';

const categories = [
  {
    name: "Hoby",
    description: "Berbagai macam barang untuk hoby anda."
  },
  {
    name: "Kendaraan",
    description: "Berbagai macam kendaraan yang bisa membawa anda pergi jauh."
  },
  {
    name: "Baju",
    description: "Berbagai macam baju untuk tampil kece setiap hari"
  },
  {
    name: "Elektronik",
    description: "Berbagai macam alat elektronik yang membantu produktifitas anda"
  },
  {
    name: "Kesehatan",
    description: "Berbagai macam alat kesehatan untuk menunjang hidup anda lebih baik"
  }
]

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const timestamp = new Date()

    const data = categories.map((category) => ({
      name: category.name,
      description: category.description,
      createdAt: timestamp,
      updatedAt: timestamp,
    }))

    await queryInterface.bulkInsert("categories", data, {})

  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("categories", null, {})
  }
};
